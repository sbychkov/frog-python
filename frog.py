import sys


def state(lamps):
    result = ""
    for lamp in lamps:
        result += ("1" if lamp else "0") + " "
    return result


def jump(max_frog_number, current_frog=1, lamps=[False, False, False, False, False]):
    if current_frog <= max_frog_number:
        index = -1
        while index + current_frog < 5:
            index += current_frog
            lamps[index] = not lamps[index]
        print ('Lamps after frog number ' + str(current_frog) + ' jumps: ' + state(lamps))
        jump(max_frog_number, current_frog + 1, lamps)


try:
    number_of_frogs = int(sys.argv[1])
    if number_of_frogs > 0 & number_of_frogs <= 5:
        jump(number_of_frogs)
    else:
        print("Wrong frog number - should be from 1 to 5")
except IndexError:
    # Five frogs jumps by default
    jump(5)
except TypeError:
    print("Argument is not a digit")
except Exception as ex:
    print("Unexpected exception: " + str(ex))
